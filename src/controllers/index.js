const EnderecoController = require('./enderecoController');
const PerfilController = require('./perfilController');
const UsuarioController = require('./usuarioController');

module.exports = {
  Endereco: EnderecoController,
  Perfil: PerfilController,
  Usuario: UsuarioController,
};

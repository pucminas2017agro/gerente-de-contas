const Controller = require('./controller');

const instance = null;

class PerfilController extends Controller {
  constructor() {
    super(db.Perfil);
    
    if (instance === null) {
      this.instance = this;
    }
  }
}

module.exports = PerfilController;

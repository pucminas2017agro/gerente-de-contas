const Controller = require('./controller');

const instance = null;

class UsuarioController extends Controller {
  constructor() {
    super(db.Usuario);

    this.models = db.Usuario;

    if (instance === null) {
      this.instance = this;
    }
  }

  autenticar(req, res) {
    if (!req.body.email && !req.body.senha) {
      return res.status(401).send({msg: 'Usuário não autenticado'})
    }

    const email = req.body.email;
    const senha = req.body.senha;

    this.models.findOne({
      attributes: ['id', 'senha'],
      where: { email: email }
    })
    .then((result) => {
      const usuario = result.dataValues;

      if (db.Usuario.isPassword(usuario.senha, senha)) {
        return res.status(200).send(usuario);
      }

      return res.status(404).send({msg: 'Usuário ou Senha Inválido'});

    })
    .catch((error) => {
      return res.status(400).send({msg: 'Error'});
    })
  }
}

module.exports = UsuarioController;

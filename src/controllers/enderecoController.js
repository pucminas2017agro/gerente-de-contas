const Controller = require('./controller');

const instance = null;

class EnderecoController extends Controller {
  constructor() {
    super(db.Endereco);

    if (instance === null) {
      this.instance = this;
    }
  }
}

module.exports = EnderecoController;

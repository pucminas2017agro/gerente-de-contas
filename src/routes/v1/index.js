const express = require('express');
const apiRoute = require('./api.routes');
const enderecoRoute = require('./enderecoRoutes');
const perfilRoute = require('./perfilRoutes');
const usuarioRoute = require('./usuarioRoutes');

const router = express.Router();

router.use('/', apiRoute);
router.use('/endereco', enderecoRoute);
router.use('/perfil', perfilRoute);
router.use('/usuario', usuarioRoute);

module.exports = router;

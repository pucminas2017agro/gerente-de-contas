const express = require('express');
const Controller = require('../../controllers/index').Usuario;

const router = express.Router();
const controller = new Controller();

router
  .route('/autenticar')
  .post((req, res) => controller.autenticar(req, res));
router
  .route('/')
  .get((req, res) => controller.list(req, res))
  .post((req, res) => controller.create(req, res));

router
  .route('/:id')
  .get((req, res) => controller.retrieve(req, res))
  .put((req, res) => controller.update(req, res))
  .delete((req, res) => controller.destroy(req, res));

module.exports = router;

const bcrypt = require('bcrypt');

module.exports = (sequelize, DataTypes) => {
  const Model = sequelize.define('Usuario', {
    id: {
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4,
      primaryKey: true
    },
    id_perfil: {
      type: DataTypes.UUID,
      references: {
        model: 'perfil',
        key: 'id',
      },
    },
    email: {
      type: DataTypes.STRING(45),
      allowNull: true,
      unique: true,
    },
    senha: {
      type: DataTypes.STRING(100),
      allowNull: true,
    },
    fb_token: {
      type: DataTypes.STRING(60),
      allowNull: true,
    },
    fb_id: {
      type: DataTypes.STRING(60),
      allowNull: true,
    },
    uuid: {
      type: DataTypes.STRING(45),
      allowNull: true,
    },
    telefone: {
      type: DataTypes.STRING(14),
      allowNull: true,
    },
    cpf: {
      type: DataTypes.STRING(45),
      allowNull: true,
    },
    nome: {
      type: DataTypes.STRING(45),
      allowNull: true,
    },
  }, {
    tableName: 'usuario',
    freezeTableName: true,
    timestamps: true,
    createdAt: 'data_criacao',
    updatedAt: 'data_atualizacao',
    hooks: {
      beforeCreate: (usuario) => {
        const salt = bcrypt.genSaltSync();
        if (usuario.senha !== undefined) {
          usuario.set('senha', bcrypt.hashSync(usuario.senha, salt));
        }
      },
    },
  });

  Model.associate = (models) => {
    console.log(`UsuarioAssociate: ${models}`);
  }

  Model.isPassword = (senhaEncriptada, senha) => bcrypt.compareSync(senha, senhaEncriptada);

  return Model;
}

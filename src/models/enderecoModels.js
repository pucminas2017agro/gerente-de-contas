module.exports = (sequelize, DataTypes) => {
  const Model = sequelize.define('Endereco', {
    id: {
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4,
      primaryKey: true
    },
    id_usuario: {
      type: DataTypes.UUID,
      references: {
        model: 'usuario',
        key: 'id',
      },
    },
    lat: {
      type: DataTypes.STRING(45),
      allowNull: true,
    },
    lon: {
      type: DataTypes.STRING(45),
      allowNull: true,
    },
    cep: {
      type: DataTypes.STRING(45),
      allowNull: true,
    },
    logradouro: {
      type: DataTypes.STRING(45),
      allowNull: true,
    },
    numero: {
      type: DataTypes.STRING(45),
      allowNull: true,
    },
    complemento: {
      type: DataTypes.STRING(45),
      allowNull: true,
    },
    bairro: {
      type: DataTypes.STRING(45),
      allowNull: true,
    },
    cidade: {
      type: DataTypes.STRING(45),
      allowNull: true,
    },
    estado: {
      type: DataTypes.STRING(45),
      allowNull: true,
    },
    referencia: {
      type: DataTypes.STRING(45),
      allowNull: true,
    },
  }, {
    tableName: 'endereco',
    freezeTableName: true,
    timestamps: true,
    createdAt: 'data_criacao',
    updatedAt: 'data_atualizacao',
  });

  Model.associate = (models) => {
    console.log(`EnderecoAssociate: ${models}`);
  }

  return Model;
}
  

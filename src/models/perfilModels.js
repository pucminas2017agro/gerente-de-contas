module.exports = (sequelize, DataTypes) => {
  const Model = sequelize.define('Perfil', {
    id: {
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4,
      primaryKey: true
    },
    descricao: {
      type: DataTypes.STRING(20),
      allowNull: false,
    },
  }, {
    tableName: 'perfil',
    freezeTableName: true,
    timestamps: true,
    createdAt: 'data_criacao',
    updatedAt: 'data_atualizacao',
  });

  Model.associate = (models) => {
    console.log(`PerfilAssociate: ${models}`);
  }

  Model.prototype.teste = () => "teste"

  return Model;

};
